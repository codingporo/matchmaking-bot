const { Client, MessageEmbed } = require('discord.js');
const client = new Client();

const token = 'NzE0NDI0NDM1MDQ1ODI2NjQx.Xs_wYQ.LDlUzJ_R0MoCpvrLRcrT9RWOOtQ'

const prefix = '!';

client.login(token);

let users = []
const WorkingChannel = "matchmaking-bot"

client.on('ready', () => {
    console.log(`${client.user.tag} is online and ready. Let's MATCHMAKE!`);
    client.channels.cache.get('721457165512081500').send('Je suis en ligne ! \n\n`!help` pour obtenir la liste des commandes disponibles.')
});

client.on('guildMemberAdd', member => {
    client.channels.cache.get('721457165512081500').send(`Bienvenue ${member} ! Tu peux utiliser ce channel pour trouver du monde pour jouer. Essaye maintenant avec !normal :)`); 
});

client.on('message', msg => {
    let args = msg.content.substring(prefix.length).split(" ")
    let link;
    if (msg.channel.name === WorkingChannel)
        switch (args[0]) {
            case 'normale':
            case 'normal':
                if (msg.mentions.users.first() && msg.member.roles.cache.find(r => r.name === "Administrateurs")) {
                    if (users.find(user => user.username == msg.mentions.users.first())) {
                        msg.channel.send(`${msg.mentions.users.first()} est déjà inscrit.`)
                        break
                    }
                    users.push(msg.mentions.users.first())
                    if (users.length === 5) {
                        msg.guild.channels.create('VALORANT - Normal', { type: "voice" }).then(channel => {
                            channel.setUserLimit(5, ["Valorant channel"]),
                                channel.setParent('697090282008608808'),
                                channel.createInvite()
                                    .then(invite => {
                                        for (var i = 0 in users)
                                            users[i].send(`Ta partie est prête ! Rejoins tes mates http://discord.gg/${invite.code}`)
                                        users = [];
                                    })
                                    .catch(console.error);
                        })
                        msg.channel.send(`Une partie a été trouvée !`)
                    }
                    msg.channel.send(`${msg.mentions.users.first()} est maintenant inscrit en matchmaking normal.`)
                    break
                }
                else if (args[1]) {
                    msg.channel.send(`Seuls les administrateurs peuvent utiliser cette commande !`)
                    break
                }
                if (users.find(user => user.username == msg.author.username)) {
                    msg.reply('Tu es déjà inscrit.')
                    break
                }
                users.push(msg.author)
                if (users.length === 5) {
                    msg.guild.channels.create('VALORANT - Normal', { type: "voice" }).then(channel => {
                        channel.setUserLimit(5, ["Valorant channel"]),
                            channel.setParent('697090282008608808'),
                            channel.createInvite()
                                .then(invite => {
                                    for (var i = 0 in users)
                                        users[i].send(`Ta partie est prête ! Rejoins tes mates http://discord.gg/${invite.code}`)
                                    users = [];
                                })
                                .catch(console.error);
                    })
                }
                msg.reply(`te voilà inscrit(e) à la file de matchmaking. Surveille tes messages privés ! Actuellement en queue : ${users.length} personne${users.length > 1 ? 's' : ''}`);
                break
            case 'leave':
            case 'quit':
                if (args[1] && msg.member.roles.cache.find(r => r.name === "Administrateurs")) {
                    if (users.find(user => user.username == msg.author.username)) {
                        const userId = users.findIndex(user => user.username == args[1])
                        users.splice(userId, 1)
                        msg.channel.send(`${args[1]} a été retiré du matchmaking.`)
                        break
                    }
                }
                else if (args[1]) {
                    msg.channel.send(`Seuls les administrateurs peuvent utiliser cette commande !`)
                    break
                }
                if (users.find(user => user.username == msg.author.username)) {
                    const userId = users.findIndex(user => user.username == msg.author.username)
                    users.splice(userId, 1)
                    msg.reply('tu as quitté la file de matchmaking normal.')
                    break
                }
                msg.reply("tu n'es pas actuellement en file d'attente.")
                break
            case 'file':
            case 'queue':
                let queue = []
                users.length === 0 ? queue[0] = "Personne pour l'instant." : queue = users
                const QueueEmbed = new MessageEmbed()
                    .setTitle("File d'attente")
                    .setColor(0xff4655).
                    setDescription(queue)

                msg.channel.send(QueueEmbed)
                break
            case 'aide':
            case 'help':
                const Embed = new MessageEmbed()
                    .setTitle("Commandes")
                    .setColor(0xff4655).
                    setDescription(
                        "`!normal` Rejoindre la file d'attente\n `!queue` Affiche la liste des utilisateurs en file d'attente \n `!quit` Quitter la file \n `!support` Obtenir le lien du support Valorant \n `!config` Liste des configurations recommandées par Riot \n \n En cas du moindre soucis avec le bot, veuillez MP <@134982177225048064>")
                msg.channel.send(Embed)
                break
            case 'support':
                msg.channel.send("Tu peux envoyer un message au support ici : https://support-valorant.riotgames.com/hc/fr/requests/new")
                break
            case 'config':
            case 'opti':
                const FirstEmbed = new MessageEmbed()
                    .setTitle("Configuration MINIMALE - 30 IMAGES PAR SECONDE ")
                    .setColor(0xff4655)
                    .setDescription('Windows 7/8/10 64 bits\nProcesseur : Intel Core 2 Duo E8400\nCarte graphique : Intel HD 4000\n4 Go de RAM\n1 GO de VRAM')
                const SecondEmbed = new MessageEmbed()
                    .setTitle("Configuration RECOMMANDÉE - 60 IMAGES PAR SECONDE")
                    .setColor(0xfff700)
                    .setDescription('Windows 7/8/10 64 bits\nProcesseur : Intel i3-4150\nCarte graphique : Geforce GT 730\n4 Go de RAM\n1 GO de VRAM')
                const ThirdEmbed = new MessageEmbed()
                    .setTitle("Configuration HAUTE PERFORMANCE - 144+ IMAGES PAR SECONDE")
                    .setColor(0x3daf02)
                    .setDescription('Windows 7/8/10 64 bits\nProcesseur : Intel Core i5-4460 3,2 GHz\nCarte graphique : GTX 1050 TI\n4 Go de RAM\n1 GO de VRAM')
                msg.channel.send(FirstEmbed)
                msg.channel.send(SecondEmbed)
                msg.channel.send(ThirdEmbed)
                break
            default:
                if (msg.author.id !== '714424435045826641')
                    msg.channel.bulkDelete(1)
                break
        }
    switch (args[0]) {
        case 'clear':
            if (!args[1] && msg.member.roles.cache.find(r => r.name !== "Administrateurs")) return false
            msg.channel.bulkDelete(args[1])
            break
    }
});
